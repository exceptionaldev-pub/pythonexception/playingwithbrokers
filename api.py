import paho.mqtt.client as pmqtt
import publisher
import topics
import os

load_dotenv()


def subscribe(host="", port=1883, topic=('/default', 0)):
    client = pmqtt.Client()

    def on_connect(client, userdata, dict, rc):
        print('Connected with result code ' + str(rc))
        client.subscribe(topic)

    client.on_connect = on_connect

    def on_message(client, userdata, msg):
        print("__________________\n")
        print('Topic: ', msg.topic + '\nMessage: ' + str(msg.payload))
        print("__________________\n")

    client.on_message = on_message

    def on_log(client, userdata, level, buf):
        print("log: ", buf)

    client.on_log = on_log
    # client.username_pw_set(username, password)
    client.connect(host, port)

    client.loop_forever()


subscribe(host=os.getenv('online_host'), port=os.getenv('online_port'), topic=topics.api_sub_topic)
