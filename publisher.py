import paho.mqtt.client as mqtt


def publish(host="", port=, topic='/default', msg="", client_id="default", username="", password="", qos=2):
    client = mqtt.Client(client_id)

    # create function to callback
    def on_publish(client, userdata, result):
        print("\ndata published on:\n" + host + ":" + str(port) + "\nTopic: " + topic)

    # assign function to callback
    client.on_publish = on_publish

    def on_log(client, userdata, level, buf):
        print("log: ", buf)

    client.on_log = on_log

    client.username_pw_set(username, password)

    client.connect(host, port)
    client.loop_start()
    client.publish(topic, msg, qos)
    client.loop_stop()

