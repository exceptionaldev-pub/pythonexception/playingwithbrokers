import paho.mqtt.client as pmqtt
import publisher
import topics
import os
from database import db
from time import strftime

load_dotenv()


def subscribe(host="", port=, topic=None):
    if topic is None:
        topic = [('/default', 0)]
    client = pmqtt.Client()

    def on_connect(client, userdata, dict, rc):
        print('Connected with result code ' + str(rc))
        client.subscribe(topic)

    client.on_connect = on_connect

    def on_message(client, userdata, msg):
        print("__________________\n")
        print('Topic: ', msg.topic + '\nMessage: ' + str(msg.payload))
        print("__________________\n")
        db.add_log(msg.topic, msg.payload, "sub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 0)

        if "LBr/" in msg.topic:
            new_topic = msg.topic[4:]
            try:
                publisher.publish(host=os.getenv('local_host'), port=os.getenv('local_port'), topic=new_topic,
                                  msg=msg.payload,
                                  client_id="LBr_o", username=os.getenv('local_broker_user'),
                                  password=os.getenv('local_broker_pass'), qos=2)
                db.add_log(new_topic, msg.payload, "pub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 1)
            except Exception:
                db.add_log(new_topic, msg.payload, "pub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 1)

    client.on_message = on_message

    def on_log(client, userdata, level, buf):
        print("log: ", buf)

    client.on_log = on_log
    client.connect(host, port)

    client.loop_forever()


subscribe(host=os.getenv('online_host'), port=os.getenv('online_port'), topic=topics.lb_sub_topic)
