import paho.mqtt.client as pmqtt
import publisher
import topics
import os
from database import db
from time import strftime

load_dotenv()


def subscribe(host="", port=, topic=None):
    if topic is None:
        topic = [('/default', 0)]
    client = pmqtt.Client()

    def on_connect(client, userdata, dict, rc):
        print('Connected with result code ' + str(rc))
        client.subscribe(topic)

    client.on_connect = on_connect

    def on_message(client, userdata, msg):
        print("__________________\n")
        print('Topic: ', msg.topic + '\nMessage: ' + str(msg.payload))
        print("__________________\n")

        if "API/" in msg.topic:
            new_topic = 'LBr/' + msg.topic[4:]
            try:
                publisher.publish(host=os.getenv('online_host'), port=os.getenv('online_port'), topic=new_topic,
                                  msg=msg.payload,
                                  client_id="OBr",
                                  qos=2)
                db.add_log(msg.topic[4:], msg.payload, "pub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 1)
            except Exception:
                db.add_log(msg.topic[4:], msg.payload, "pub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 1)

        elif "LBr/" in msg.topic:
            db.update_flag(msg.topic[4:-7], msg.payload)

            if "present" in msg.payload:
                presentation(msg)

            else:
                if "door" in msg.topic:
                    thing = "door"
                    thing_nr = 0
                    last_status = msg.payload
                else:
                    topic_splited = msg.topic.split('/')
                    thing = topic_splited[3]
                    thing_nr = topic_splited[4]
                    last_status = msg.payload
                db.add_status(thing, thing_nr, last_status, strftime("%Y-%m-%d"), strftime("%H:%M:%S"))

            new_topic = 'API/' + msg.topic[4:]
            try:
                publisher.publish(host=os.getenv('online_host'), port=os.getenv('online_port'), topic=new_topic,
                                  msg=msg.payload,
                                  client_id="OBr", qos=2)
            except Exception:
                pass

    client.on_message = on_message

    def on_log(client, userdata, level, buf):
        print("log: ", buf)

    client.on_log = on_log
    client.connect(host, port)

    client.loop_forever()


def presentation(msg):
    msg_decoded = msg.payload.decode("utf-8")
    msg_array = msg_decoded.split('/')
    id = msg_array[0]
    date = msg_array[1]
    time = msg_array[2]
    module = msg_array[3]
    status = msg_array[4]
    add_present(msg, int(id), date, time, module, status)


def add_present(msg, id, date, time, module, status):
    present = db.get_last_present(id, date)
    if present is not None:
        if present.status == status:
            db.update_last_present(present.id_present, time)
        else:
            db.add_present(id, date, time, module, status)
    else:
        db.add_present(id, date, time, module, status)
    db.add_present_log(id, date, time, "noId", module, status)
    print("Server Present Added!")


subscribe(host=os.getenv('online_host'), port=os.getenv('online_port'), topic=topics.ob_sub_topic)
