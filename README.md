# Playing With Brokers ;)

### Requirements:

1. Run **Mosquitto** or **EMQ** Broker on your server and your raspberry pi.

2. Install requirements packages with following commands:

    `apt install libmysqlclient-dev` (Just for debian distro!)

    `pip install mysqlclient`

    `pip install sqlalchemy`

    `pip install paho-mqtt`
    
3. Create `.env` file exactly the same of `.env.example` and fill it with your own information.

4. Run `Online_broker.py` file on your server.

5. Run both `local_broker_l.py` and `local_broker_o.py` files on your raspberry pi.

## Topics and Messages

### API -> Sensors

**Topic:** `Labs/AI/door` **Message:** `open/close`

**Topic:** `Labs/AI/Light/0` **Message:** `on/off`

**Topic:** `Labs/AI/Light/1` **Message:** `on/off`

**Topic:** `Labs/AI/Light/2` **Message:** `on/off`

**Topic:** `Labs/AI/Switch/0` **Message:** `on/off`

**Topic:** `Labs/AI/Switch/1` **Message:** `on/off`

**Topic:** `Labs/AI/Switch/2` **Message:** `on/off`

**Topic:** `Labs/AI/AirConditioner` **Message:** `on/off`

### Sensors -> API

**Topic:** `Labs/AI/door/status` **Message:** `open/close`

**Topic:** `Labs/AI/Light/0/status` **Message:** `on/off`

**Topic:** `Labs/AI/Light/1/status` **Message:** `on/off`

**Topic:** `Labs/AI/Light/2/status` **Message:** `on/off`

**Topic:** `Labs/AI/Switch/0/status` **Message:** `on/off`

**Topic:** `Labs/AI/Switch/1/status` **Message:** `on/off`

**Topic:** `Labs/AI/Switch/2/status` **Message:** `on/off`

**Topic:** `Labs/AI/AirConditioner/status` **Message:** `on/off`

**Topic:** `Labs/AI/Fire/status` **Message:** `detected`

**Topic:** `Labs/AI/CO2/status` **Message:** `CO2_Density`

**Topic:** `Labs/AI/CO4/status` **Message:** `CH4_Density`

**Topic:** `Labs/AI/Humid/status` **Message:** `Amount_of_moisture`

**Topic:** `Labs/AI/Temp/status` **Message:** `Temperature_Degree`

**Topic:** `Labs/AI/FP/EN/status` **Message:** `Fingerprint_ID` (For Enter)

**Topic:** `Labs/AI/FP/EX/status` **Message:** `Fingerprint_ID` (For Exit)

**Topic:** `Labs/AI/RFID/EN/status` **Message:** `UID` (For Enter)

**Topic:** `Labs/AI/RFID/EX/status` **Message:** `UID` (For Exit)

## Let's Play :)

<img src="brokers.png" >
