topic_list = ["Labs/AI/door",
              "Labs/AI/Light/0",
              "Labs/AI/Light/1",
              "Labs/AI/Light/2",
              "Labs/AI/Switch/0",
              "Labs/AI/Switch/1",
              "Labs/AI/Switch/2",
              "Labs/AI/AirConditioner"]

topic_list_status = ["Labs/AI/door/status",
                     "Labs/AI/Light/0/status",
                     "Labs/AI/Light/1/status",
                     "Labs/AI/Light/2/status",
                     "Labs/AI/Switch/3/status",
                     "Labs/AI/Switch/4/status",
                     "Labs/AI/Switch/5/status",
                     "Labs/AI/AirConditioner/status",
                     "Labs/AI/Fire/status",
                     "Labs/AI/Temp/status",
                     "Labs/AI/CO2/status",
                     "Labs/AI/CH4/status",
                     "Labs/AI/Humid/status",
                     "Labs/AI/FP/EN/status",
                     "Labs/AI/FP/EX/status",
                     "Labs/AI/RFID/EN/status",
                     "Labs/AI/RFID/EX/status"]

# API
api_sub_topic = []
for topic in topic_list_status:
    api_sub_topic.append((('API/' + str(topic)), 2))

api_pub_topic = []
for topic in topic_list:
    api_pub_topic.append((('API/' + str(topic)), 2))

# Local Broker
lb_sub_topic = []
for topic in topic_list:
    lb_sub_topic.append((('LBr/' + str(topic)), 2))
for topic in topic_list_status:
    lb_sub_topic.append((str(topic), 2))

lb_pub_topic = []
for topic in topic_list_status:
    lb_pub_topic.append((('LBr/' + str(topic)), 2))
for topic in topic_list:
    lb_pub_topic.append((str(topic), 2))

# Online Broker
ob_sub_topic1 = []
for topic in topic_list_status:
    ob_sub_topic1.append((('LBr/' + str(topic)), 2))
ob_sub_topic = api_pub_topic + ob_sub_topic1

ob_pub_topic1 = []
for topic in topic_list:
    ob_pub_topic1.append((('LBr/' + str(topic)), 2))
ob_pub_topic = api_sub_topic + ob_pub_topic1

# Sensor
sen_sub_topic = []
for topic in topic_list:
    sen_sub_topic.append((str(topic), 2))

sen_pub_topic = []
for topic in topic_list_status:
    sen_pub_topic.append((str(topic), 2))
