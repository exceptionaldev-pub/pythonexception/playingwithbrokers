import paho.mqtt.client as pmqtt
import topics
import publisher
import os

load_dotenv()


def subscribe(host="", port=, topic=('/default', 0), username="", password=""):
    client = pmqtt.Client()

    def on_connect(client, userdata, dict, rc):
        print('Connected with result code ' + str(rc))
        client.subscribe(topic)

    client.on_connect = on_connect

    def on_message(client, userdata, msg):
        print("__________________\n")
        print('Topic: ', msg.topic + '\nMessage: ' + str(msg.payload))
        print("__________________\n")
        new_topic = msg.topic + '/status'
        publisher.publish(host=os.getenv('local_host'), port=os.getenv('local_port'), topic=new_topic,
                          client_id="Ligth1",
                          msg=msg.payload, username=os.getenv('local_broker_user'),
                          password=os.getenv('local_broker_pass'))

    client.on_message = on_message

    def on_log(client, userdata, level, buf):
        print("log: ", buf)

    client.on_log = on_log
    client.username_pw_set(username, password)
    client.connect(host, port)

    client.loop_forever()


publisher.publish(host=os.getenv('local_host'), port=os.getenv('local_port'), topic="Labs/AI/FP/EX/status",
                  client_id="FP",
                  msg="20", username=os.getenv('local_broker_user'), password=os.getenv('local_broker_pass'))
