from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import config
from sqlalchemy import func

from database.db_model import FingerPrint, RFID, Log, Status, User, PresentHistory, PresentLog

# connection string
engine = create_engine(config.local_db_connection, pool_recycle=3600)

Session = sessionmaker(bind=engine)


def add_fp(id_fp, id_u):
    fingerprint = FingerPrint(id_fp=id_fp, id_u=id_u)
    session = Session()
    session.add(fingerprint)
    session.commit()


def find_fp(id_fp):
    try:
        return Session().query(FingerPrint.id_u) \
            .filter(FingerPrint.id_fp == id_fp).one()
    except Exception:
        pass


def add_rfid(rfid, id_u):
    ri = RFID(rfid=rfid, id_u=id_u)
    Session().add(ri)
    Session().commit()


def find_rfid(rfid):
    try:
        return Session().query(RFID.id_u) \
            .filter(RFID.rfid == rfid).one()
    except Exception:
        pass


def find_name(id_u):
    try:
        return Session().query(User.firstname) \
            .filter(User.id_user == id_u).one()
    except Exception:
        pass


def check_permission(id_u):
    try:
        return Session().query(User.en_permission) \
            .filter(User.id_user == id_u).one()
    except Exception:
        pass


def get_last_present(id_u, date):
    try:
        maxPresent = Session().query(func.max(PresentHistory.id_present)) \
            .filter(PresentHistory.id_user == id_u).one()
        return Session().query(PresentHistory) \
            .filter(PresentHistory.date == date, PresentHistory.id_user == id_u,
                    PresentHistory.id_present == maxPresent).one()
    except Exception:
        pass


def update_last_present(id_p, time):
    try:
        session = Session()
        session.query(PresentHistory) \
            .filter(PresentHistory.id_present == id_p).update({PresentHistory.time: time})
        session.commit()
    except Exception:
        pass


def add_present(id_u, date, time, module, status):
    ph = PresentHistory(id_user=id_u, date=date, time=time, module=module, status=status)
    session = Session()
    session.add(ph)
    session.commit()


def add_present_log(id_u, date, time, id_module, module, status):
    pl = PresentLog(id_user=id_u, date=date, time=time, id_module=id_module, module=module, status=status)
    session = Session()
    session.add(pl)
    session.commit()


def num_of_enter(date):
    try:
        return Session().query(PresentHistory) \
            .filter(PresentHistory.date == date, PresentHistory.status == "EN").count()
    except Exception:
        pass


def num_of_exit(date):
    try:
        return Session().query(PresentHistory) \
            .filter(PresentHistory.date == date, PresentHistory.status == "EX").count()
    except Exception:
        pass


def add_log(topic, msg, type, date, time, exit_code):
    log = Log(topic=topic, msg=msg, type=type, date=date, time=time, exit_code=exit_code)
    session = Session()
    session.add(log)
    session.commit()


def add_status(thing, thing_nr, last_status, date, time):
    status = Status(thing=thing, thing_nr=thing_nr, last_status=last_status, date=date, time=time)
    session = Session()
    session.add(status)
    session.commit()


def update_flag(topic, msg):
    try:
        session = Session()
        session.query(Log) \
            .filter(Log.topic == topic, Log.msg == msg, Log.exit_code == 1).update({Log.exit_code: 0})
        session.commit()
    except Exception:
        pass
