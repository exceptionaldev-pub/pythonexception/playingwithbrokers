# coding: utf-8
from sqlalchemy import Column
from sqlalchemy.dialects.mysql import INTEGER, VARCHAR, DATETIME, TINYINT, TIME, DATE
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
# metadata = Base.metadata


class User(Base):
    __tablename__ = 'users'

    id_user = Column(INTEGER(10), primary_key=True, index=True)
    username = Column(VARCHAR(15), nullable=False, index=True)
    firstname = Column(VARCHAR(199), nullable=False, index=True)
    lastname = Column(VARCHAR(199), nullable=False, index=True)
    password = Column(VARCHAR(199), nullable=False, index=True)
    rank = Column(VARCHAR(199), nullable=True, index=True)
    en_permission = Column(TINYINT(1), nullable=False, index=True)
    email = Column(VARCHAR(100), nullable=True, index=True)
    active = Column(TINYINT(1), nullable=False, index=True)
    time_add = Column(DATETIME(6), nullable=False, index=True)
    time_upd = Column(DATETIME(6), nullable=False, index=True)


class FingerPrint(Base):
    __tablename__ = 'fingerprint'

    id_fp = Column(INTEGER(10), primary_key=True, index=True)
    id_u = Column(INTEGER(4), nullable=False, index=True)


class RFID(Base):
    __tablename__ = 'rfid'

    id_rf = Column(INTEGER(100), primary_key=True, index=True)
    id_u = Column(INTEGER(4), nullable=False, index=True)
    rfid = Column(VARCHAR(1000), nullable=False, index=True)


class PresentHistory(Base):
    __tablename__ = 'present_history'

    id_present = Column(INTEGER(100), primary_key=True, index=True)
    id_user = Column(INTEGER(4), nullable=False, index=True)
    date = Column(DATE(), nullable=False, index=True)
    time = Column(TIME(), nullable=False, index=True)
    module = Column(VARCHAR(5), nullable=False, index=True)
    status = Column(VARCHAR(3), nullable=False, index=True)


class PresentLog(Base):
    __tablename__ = 'present_log'

    id_present = Column(INTEGER(100), primary_key=True, index=True)
    id_user = Column(INTEGER(4), nullable=True, index=True)
    id_module = Column(VARCHAR(10), nullable=True, index=True)
    date = Column(DATE(), nullable=False, index=True)
    time = Column(TIME(), nullable=False, index=True)
    module = Column(VARCHAR(5), nullable=False, index=True)
    status = Column(VARCHAR(3), nullable=False, index=True)


class Log(Base):
    __tablename__ = 'log'

    id_m = Column(INTEGER(100), primary_key=True, index=True)
    topic = Column(VARCHAR(100), nullable=False, index=True)
    msg = Column(VARCHAR(100), nullable=False, index=True)
    type = Column(VARCHAR(3), nullable=False, index=True)
    date = Column(DATE(), nullable=False, index=True)
    time = Column(TIME(), nullable=False, index=True)
    exit_code = Column(INTEGER(2), nullable=False, index=True)


class Status(Base):
    __tablename__ = 'status'

    id_s = Column(INTEGER(100), primary_key=True, index=True)
    thing = Column(VARCHAR(10), nullable=False, index=True)
    thing_nr = Column(VARCHAR(3), nullable=False, index=True)
    last_status = Column(VARCHAR(10), nullable=False, index=True)
    date = Column(DATE(), nullable=False, index=True)
    time = Column(TIME(), nullable=False, index=True)
