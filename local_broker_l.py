import paho.mqtt.client as pmqtt
import requests
from time import strftime
from database import db
import publisher
import topics
import os

load_dotenv()


def subscribe(host="", port=, topic=None, username="", password=""):
    if topic is None:
        topic = [('/default', 0)]
    client = pmqtt.Client()

    def on_connect(client, userdata, dict, rc):
        print('Connected with result code ' + str(rc))
        client.subscribe(topic)

    client.on_connect = on_connect

    def on_message(client, userdata, msg):
        print("__________________\n")
        print('Topic: ', msg.topic + '\nMessage: ' + str(msg.payload))
        print("__________________\n")

        if any(c in msg.topic for c in ("FP", "RFID")):
            presentation(msg)

        if "/status" in msg.topic:
            db.update_flag(msg.topic[:-7], msg.payload)

            if "door" in msg.topic:
                print("Door opened!")
                thing = "door"
                thing_nr = 0
                last_status = "opened"
            else:
                topic_splited = msg.topic.split('/')
                thing = topic_splited[2]
                thing_nr = topic_splited[3]
                last_status = msg.payload
            db.add_status(thing, thing_nr, last_status, strftime("%Y-%m-%d"), strftime("%H:%M:%S"))

            new_topic = 'LBr/' + msg.topic
            try:
                publisher.publish(host=os.getenv('online_host'), port=os.getenv('online_port'), topic=new_topic,
                                  msg=msg.payload,
                                  client_id="LBr", qos=2)
            except Exception:
                db.add_log(new_topic, msg.payload, "pub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 1)

    client.on_message = on_message

    def on_log(client, userdata, level, buf):
        print("log: ", buf)

    client.on_log = on_log
    client.username_pw_set(username, password)
    client.connect(host, port)

    client.loop_forever()


def presentation(msg):
    id = None
    status = "UNKNOWN"
    date = strftime("%Y-%m-%d")
    time = strftime("%H:%M:%S")
    module = "UNKNOWN"
    if "FP" in msg.topic:
        id = db.find_fp(int(msg.payload))
        module = "FP"
    elif "RFID" in msg.topic:
        id = db.find_rfid(msg.payload)
        module = "RFID"
    if id is not None:
        open_door(id)
        name = db.find_name(id)
        print("Welcome " + name[0] + "!")
        if "EN" in msg.topic:
            if db.num_of_enter(date) - db.num_of_exit(date) == 0:
                turn_on_first_light()
            status = "EN"
        else:
            if db.num_of_enter(date) - db.num_of_exit(date) == 1:
                turn_off_all_light()
            status = "EX"
        add_present(msg, id, date, time, module, status)
    else:
        db.add_present_log(id, date, time, msg.payload, module, status)


def open_door(id_u):
    permission = db.check_permission(id_u)
    if permission[0] == 1:
        try:
            publisher.publish(host=config.local_host, port=config.local_port, topic="Labs/AI/door",
                              msg="open", client_id="LBr", username=config.local_broker_user,
                              password=config.local_broker_pass, qos=2)
            db.add_log("Labs/AI/door", "open", "pub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 1)
        except Exception:
            db.add_log("Labs/AI/door", "open", "pub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 1)


def turn_on_first_light():
    try:
        publisher.publish(host=config.local_host, port=config.local_port, topic="Labs/AI/Light/0", msg="on",
                          client_id="LBr", username=config.local_broker_user, password=config.local_broker_pass,
                          qos=2)
        db.add_log("Labs/AI/Light/0", "on", "pub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 1)
    except Exception:
        db.add_log("Labs/AI/Light/0", "on", "pub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 1)


def turn_off_all_light():
    try:
        publisher.publish(host=config.local_host, port=config.local_port, topic="Labs/AI/Light/0", msg="off",
                          client_id="LBr", username=config.local_broker_user, password=config.local_broker_pass, qos=2)
        db.add_log("Labs/AI/Light/0", "off", "pub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 1)

        publisher.publish(host=config.local_host, port=config.local_port, topic="Labs/AI/Light/1", msg="off",
                          client_id="LBr", username=config.local_broker_user, password=config.local_broker_pass, qos=2)
        db.add_log("Labs/AI/Light/1", "off", "pub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 1)

        publisher.publish(host=config.local_host, port=config.local_port, topic="Labs/AI/Light/2", msg="off",
                          client_id="LBr", username=config.local_broker_user, password=config.local_broker_pass, qos=2)
        db.add_log("Labs/AI/Light/2", "off", "pub", strftime("%Y-%m-%d"), strftime("%H:%M:%S"), 1)

    except Exception:
        pass


def add_present(msg, id, date, time, module, status):
    present = db.get_last_present(id, date)
    if present is not None:
        if present.status == status:
            db.update_last_present(present.id_present, time)
        else:
            db.add_present(id, date, time, module, status)
    else:
        db.add_present(id, date, time, module, status)
    db.add_present_log(id, date, time, msg.payload, module, status)
    print("Local Present Added!")
    req(id, module, status, date, time)
    send_to_server(msg, id, date, time, module, status)


def send_to_server(msg, id, date, time, module, status):
    new_topic = 'LBr/' + msg.topic
    try:
        publisher.publish(host=config.online_host, port=config.online_port, topic=new_topic,
                          msg=(str(id[0]) + "/" + date + "/" + time + "/" + module + "/" + status + "/present"),
                          client_id="LBr", qos=2)
    except Exception:
        pass


def req(id, module, status, date, time):
    content = {
        'function': "present_module",
        'id_user': id,
        'module': module,
        'status': status,
        'date': date,
        'time': time
    }

    try:
        r = requests.post("https://<URL>", json=content)
        print(r.text[:300])
    except Exception:
        pass


subscribe(host=config.local_host, port=config.local_port, topic=topics.lb_sub_topic, username=config.local_broker_user,
          password=config.local_broker_pass)
